﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BotApplicationDemo.Common
{
    [Serializable]
    public class CaseDetail
    {
        public string categoryName { get; set; }
        public string categoryCode { get; set; }
        public JArray categoryItemResults { get; set; }
        public string problemString { get; set; }
        public JArray kbItemResults { get; set; }
        public int kbItemResultsIndex { get; set; }
        public string productNumber { get; set; }
        public Guid productId { get; set; }
    }
}