﻿using Microsoft.Azure.CognitiveServices.Language.TextAnalytics;
using Microsoft.Azure.CognitiveServices.Language.TextAnalytics.Models;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Builder.FormFlow.Json;
using Microsoft.Bot.Connector;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Http;

namespace BotApplicationDemo.Common
{
    [Serializable]
    public class Utilities
    {
        public static string StripHTML(string HTMLText)
        {
            Regex reg = new Regex("<.*?>", RegexOptions.IgnoreCase);
            var stripped = reg.Replace(HTMLText, "");
            return stripped.Trim();
        }

        [BotAuthentication]
        [Authorize]
        public async Task<HttpResponseMessage> CRMWebAPIRequest(string apiRequest, HttpContent requestContent, string requestType)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            AuthenticationContext authContext = new AuthenticationContext(WebConfigurationManager.AppSettings["adOath2AuthEndpoint"], false);
            UserCredential credentials = new UserCredential(WebConfigurationManager.AppSettings["crmUsername"],
                WebConfigurationManager.AppSettings["crmPassword"]);
            AuthenticationResult tokenResult = authContext.AcquireToken(WebConfigurationManager.AppSettings["crmUri"],
                WebConfigurationManager.AppSettings["adClientId"], credentials);
            HttpResponseMessage apiResponse;

            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(WebConfigurationManager.AppSettings["crmUri"]);
                httpClient.Timeout = new TimeSpan(0, 2, 0);
                httpClient.DefaultRequestHeaders.Add("OData-MaxVersion", "4.0");
                httpClient.DefaultRequestHeaders.Add("OData-Version", "4.0");
                httpClient.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("Bearer", tokenResult.AccessToken);

                if (requestType == "retrieve")
                {
                    apiResponse = await httpClient.GetAsync(apiRequest);
                }
                else if (requestType == "create")
                {
                    apiResponse = await httpClient.PostAsync(apiRequest, requestContent);
                }
                else
                {
                    apiResponse = null;
                }
            }
            return apiResponse;
        }

        public static Double TextAnalytics(IDialogContext context, string keyword)
        {
            ITextAnalyticsClient client = new TextAnalyticsClient(new ApiKeyServiceClientCredentials())
            {
                BaseUri = new Uri(WebConfigurationManager.AppSettings["TextAnalyticsEndpoint"])
            };

            try
            {
                var result = client.SentimentAsync(new MultiLanguageBatchInput(
                        new List<MultiLanguageInput>()
                        {
                          new MultiLanguageInput("en", "0", keyword)
                        })).Result;

                if (result.Documents.Count > 0)
                {
                    return Math.Round(Convert.ToDouble(result.Documents[0].Score), 2);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return 0;
        }

        public static IForm<JObject> BuildJsonForm()
        {
            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("BotApplicationDemo.JsonSchema.CreateIncident_Schema.json"))
            {
                var schema = JObject.Parse(new StreamReader(stream).ReadToEnd());
                return new FormBuilderJson(schema)
                    .AddRemainingFields()
                    .Build();
            }
        }

        //public static async Task SubmitCaseAsync(IDialogContext context, CaseDetail caseDetail)
        //{
        //    Guid contactid = new Guid("5259B308-A2B1-E811-A962-000D3A3740B7");

        //    try
        //    {
        //        var stringContent = new StringContent("{" +
        //            "\"title\":\"" + caseDetail.categoryCode + " - " + caseDetail.productNumber + "\"," +
        //            "\"description\":\"" + caseDetail.problemString + "\"," +
        //            "\"customerid_contact@odata.bind\":\"/contacts(" + contactid.ToString() + ")\"" +
        //            "}", System.Text.Encoding.UTF8, "application/json");

        //        HttpResponseMessage caseResponse = await CRMWebAPIRequest("/api/data/v9.0/incidents", stringContent, "create");

        //        if (caseResponse.IsSuccessStatusCode)
        //        {
        //            Guid caseId = new Guid();
        //            string caseDetailsRequest = "";
        //            string caseUri = caseResponse.Headers.GetValues("OData-EntityId").FirstOrDefault();
        //            if (caseUri != null)
        //            {
        //                caseId = Guid.Parse(caseUri.Split('(', ')')[1]);
        //                caseDetailsRequest = "api/data/v9.0/incidents(" +
        //                    caseId + ")?" + "$select=ticketnumber";

        //                HttpResponseMessage caseDetailsResponse = await CRMWebAPIRequest(caseDetailsRequest, null, "retrieve");

        //                if (caseDetailsResponse.IsSuccessStatusCode)
        //                {
        //                    string myString = caseDetailsResponse.Content.ReadAsStringAsync().Result;
        //                    JObject caseResults =
        //                        JObject.Parse(caseDetailsResponse.Content.ReadAsStringAsync().Result);
        //                    string ticketNumber = (string)caseResults["ticketnumber"];

        //                    await context.PostAsync("Your issue has been submitted. Your Case Number is: __" + ticketNumber + "__ .");
        //                }
        //                else
        //                {
        //                    await context.PostAsync("An error occurred while submitting your issue.");
        //                }
        //            }
        //            else
        //            {
        //                await context.PostAsync("An error occurred while submitting your issue.");
        //            }
        //        }
        //    }
        //    catch (FormCanceledException<JObject> e)
        //    {
        //        string reply;
        //        if (e.InnerException == null)
        //        {
        //            reply = $"You quit on {e.Last}--maybe you can finish next time!";
        //        }
        //        else
        //        {
        //            reply = "Sorry, I've had a short circuit.  Please try again.";
        //        }

        //        await context.PostAsync(reply);
        //    }

        //    await context.PostAsync("If you need any assistance in the future, you know where to find me.");
        //    context.Done<bool>(true);
        //}

        //public static async Task SubmitCaseDeflection(IDialogContext context, CaseDetail caseDetail)
        //{
        //    Guid contactid = new Guid("5259B308-A2B1-E811-A962-000D3A3740B7");

        //    try
        //    {
        //        var stringContent = new StringContent("{" +
        //            "\"adx_name\":\"" + caseDetail.categoryCode + " - " + caseDetail.productNumber + "\"," +
        //            "\"a1a_description\":\"" + caseDetail.problemString + "\"," +
        //            "\"adx_Contact@odata.bind\":\"/contacts(" + contactid.ToString() + ")\"," +
        //            "\"a1a_ProductId@odata.bind\":\"/products(" + caseDetail.productId.ToString() + ")\"," +
        //            "\"adx_KnowledgeArticle@odata.bind\":\"/knowledgearticles(" + caseDetail.kbItemResults[caseDetail.kbItemResultsIndex]["knowledgearticleid"].ToString() + ")\"" +
        //            "}", System.Text.Encoding.UTF8, "application/json");

        //        var str = stringContent.ReadAsStringAsync();

        //        HttpResponseMessage caseResponse = await CRMWebAPIRequest("/api/data/v9.0/adx_casedeflections", stringContent, "create");

        //        if (caseResponse.IsSuccessStatusCode)
        //        {
        //            await context.PostAsync("Case deflection submitted");
        //        }
        //        else
        //        {
        //            await context.PostAsync("An error occurred while submitting your issue.");
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
    }
}