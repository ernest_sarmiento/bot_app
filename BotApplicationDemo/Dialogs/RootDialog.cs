﻿using BotApplicationDemo.Common;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;


namespace BotApplicationDemo.Dialogs
{
    [Serializable]
    [BotAuthentication]
    [Authorize]
    public class RootDialog : IDialog<object>
    {
        Utilities utilities = new Utilities();
        public static CaseDetail caseDetail = new CaseDetail();

        public Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);

            return Task.CompletedTask;
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
        {
            var activity = await result as Activity;

            if (activity.Text.Equals("hi") || activity.Text.Equals("hello"))
            {
                System.Collections.Generic.List<string> categoryOptions = new System.Collections.Generic.List<string>();

                HttpResponseMessage categoryResponse = await utilities.CRMWebAPIRequest("api/data/v9.0/categories?$select=categorynumber,title",
                            null, "retrieve");
                if (categoryResponse.IsSuccessStatusCode)
                {
                    string categoryResponseStr = categoryResponse.Content.ReadAsStringAsync().Result;
                    JObject categoryResults = JObject.Parse(categoryResponseStr);
                    caseDetail.categoryItemResults = (JArray)categoryResults["value"];

                    if (categoryResults.Count > 0)
                    {
                        foreach (var item in caseDetail.categoryItemResults)
                        {
                            categoryOptions.Add((string)item["title"]);
                        }
                    }
                }

                PromptDialog.Choice(context, GetCategoryAsync, new PromptOptions<string>("Hi! How can I help you?", null, null, categoryOptions, 3, null));  
            }
            else if(activity.Text.Equals("testing"))
            {
                await context.PostAsync("Testing Complete!");
            }
        }

        public async Task GetCategoryAsync(IDialogContext context, IAwaitable<object> argument)
        {
            caseDetail.categoryName = (string)await argument;

            caseDetail.categoryCode = caseDetail.categoryItemResults.Where(t => (string)t["title"] == caseDetail.categoryName).Select(t => (string)t["categorynumber"]).FirstOrDefault();

            PromptDialog.Text(context, GetProductAsync, "Can you tell me what is the printer model?", null, 3);
        }

        public async Task GetProductAsync(IDialogContext context, IAwaitable<object> argument)
        {
            caseDetail.productNumber = (string) await argument;

            HttpResponseMessage productResponse = await utilities.CRMWebAPIRequest("/api/data/v9.0/products?$select=name,productid,productnumber&$filter=productnumber eq '" + caseDetail.productNumber + "'",
                                null, "retrieve");

            if (productResponse.IsSuccessStatusCode)
            {
                string productResponseStr = productResponse.Content.ReadAsStringAsync().Result;
                JObject productResults = JObject.Parse(productResponseStr);
                JArray productItemResults = (JArray)productResults["value"];

                if (productItemResults.Count > 0)
                {
                    caseDetail.productId = new Guid(productItemResults[0]["productid"].ToString());

                    PromptDialog.Text(context, GetKBAsync, "Please provide detailed description regarding your problem.");
                }
                else
                {
                    PromptDialog.Text(context, GetProductAsync, "Sorry, I could not find that printer model. Please tell what is the correct printer model?");
                }
            }
            else
            {
                PromptDialog.Text(context, GetProductAsync, "Sorry, I could not find that printer model. Please tell what is the correct printer model?");
            }

        }

        public async Task GetKBAsync(IDialogContext context, IAwaitable<string> argument)
        {
            caseDetail.problemString = await argument;

            HttpResponseMessage kbResponse = await utilities.CRMWebAPIRequest("/api/data/v9.0/knowledgearticles?fetchXml=%3Cfetch%20version%3D%221.0%22%20output-format%3D%22xml-platform%22%20mapping%3D%22logical%22%20distinct%3D%22true%22%3E%3Centity%20name%3D%22knowledgearticle%22%3E%3Cattribute%20name%3D%22articlepublicnumber%22%20%2F%3E%3Cattribute%20name%3D%22knowledgearticleid%22%20%2F%3E%3C" +
                "attribute%20name%3D%22title%22%20%2F%3E%3Cfilter%20type%3D%22and%22%3E%3Ccondition%20attribute%3D%22isrootarticle%22%20operator%3D%22eq%22%20value%3D%220%22%20%2F%3E%3C%2Ffilter%3E%3Clink-entity%20name%3D%22connection%22%20from%3D%22record2id%22%20to%3D%22knowledgearticleid%22%20" +
                "link-type%3D%22inner%22%20alias%3D%22at%22%3E%3Clink-entity%20name%3D%22product%22%20from%3D%22productid%22%20to%3D%22record1id%22%20link-type%3D%22inner%22%20alias%3D%22au%22%3E%3Cfilter%20type%3D%22and%22%3E%3Ccondition%20attribute%3D%22productnumber%22%20operator%3D%22eq%22%20value%3D%22" +
                caseDetail.productNumber + "%22%20%2F%3E%3C%2Ffilter%3E%3C%2Flink-entity%3E%3C%2Flink-entity%3E%3Clink-entity%20name%3D%22knowledgearticlescategories%22%20from%3D%22knowledgearticleid%22%20to%3D%22knowledgearticleid%22%20visible%3D%22false%22%20intersect%3D%22true%22%3E%3Clink-entity%20name%3D%22category%22%20from%3D%22" +
                "categoryid%22%20to%3D%22categoryid%22%20alias%3D%22av%22%3E%3Cfilter%20type%3D%22and%22%3E%3Ccondition%20attribute%3D%22categorynumber%22%20operator%3D%22eq%22%20value%3D%22" + caseDetail.categoryCode + "%22%20%2F%3E%3C%2Ffilter%3E%3C%2Flink-entity%3E%3C%2Flink-entity%3E%3C%2Fentity%3E%3C%2Ffetch%3E"
                , null, "retrieve");

            if (kbResponse.IsSuccessStatusCode)
            {
                string myString = kbResponse.Content.ReadAsStringAsync().Result;
                JObject kbResults = JObject.Parse(myString);
                caseDetail.kbItemResults = (JArray)kbResults["value"];
                caseDetail.kbItemResultsIndex = 0;

                if (caseDetail.kbItemResults.Count > 0)
                {
                    IMessageActivity msgMarkdown = context.MakeMessage();
                    msgMarkdown.Text = "Please go through this steps.";
                    msgMarkdown.Text += Environment.NewLine + "[" + (string)caseDetail.kbItemResults[0]["title"] + "](" + (string)caseDetail.kbItemResults[0]["articlepublicnumber"] + ") ";

                    await context.PostAsync(msgMarkdown);

                    PromptDialog.Confirm(context, ShowNextKBAsync, "Did this solve your problem?");
                }
            }
        }

        public async Task ShowNextKBAsync(IDialogContext context, IAwaitable<bool> argument)
        {
            var option = await argument;

            if (option == false)
            {
                if (caseDetail.kbItemResultsIndex <= caseDetail.kbItemResults.Count - 1)
                {
                    IMessageActivity msgMarkdown = context.MakeMessage();
                    msgMarkdown.Text = "Please go through this steps.";
                    msgMarkdown.Text += Environment.NewLine + "[" + (string)caseDetail.kbItemResults[caseDetail.kbItemResultsIndex]["title"] + "](" + (string)caseDetail.kbItemResults[caseDetail.kbItemResultsIndex]["articlepublicnumber"] + ") ";
                    caseDetail.kbItemResultsIndex++;
                    await context.PostAsync(msgMarkdown);

                    PromptDialog.Confirm(context, ShowNextKBAsync, "Did this solve your problem?"); 
                }
                else
                {
                    await context.PostAsync("No more articles.");

                    //await Utilities.SubmitCaseAsync(context, caseDetail);
                }
            }
            else
            {
                //await Utilities.SubmitCaseDeflection(context, caseDetail);
            }
        }

    }
}